//SPDX-License-Identifier: Unlicense
pragma solidity ^0.7.0;
import "hardhat/console.sol";
import "./CompensationSplitter.sol";

contract AttributionManager{

  //address public addr = address(this);
  string public name;

  CompensationSplitter[] public splitters;
  address[] public splittersAdrresses;
  uint public totalSplitters;
  event splitterCreated( CompensationSplitter, address );

  constructor(
    string memory _name
  ) public {
    name = _name;
  }

  function setName( string memory x ) public {
    name = x;
  }

  function addSplitter(
    string memory _attributable,
    address[] memory payees,
    uint256[] memory shares  
  ) public returns ( address ) {
    // require(
    //   account != address(0),
    //   "PaymentSplitter: account is the zero address"
    // );
    // require(
    //   shares_ > 0,
    // "PaymentSplitter: shares are 0"
    // );
    // require(
    //  _shares[account] == 0,
    //  "PaymentSplitter: account already has shares"
    // );
    CompensationSplitter splitter = new CompensationSplitter(
      _attributable,
      payees,
      shares 
    );
    totalSplitters++;
    // splitters.[msg.sender].push(newContract);
    splitters.push( splitter );
    //  _payees.push(account);
    //  _shares[account] = shares_;
    //  _totalShares = _totalShares.add(shares_);

    address s = address( splitter );
    emit splitterCreated( splitter, s );
    splittersAdrresses.push( s );
    return s;
  }
}





