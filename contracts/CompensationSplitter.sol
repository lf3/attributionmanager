//SPDX-License-Identifier: Unlicense
//pragma solidity ^0.6.0;
pragma solidity ^0.7.0;
import "hardhat/console.sol";

import "@openzeppelin/contracts/payment/PaymentSplitter.sol";

contract CompensationSplitter is PaymentSplitter{

  // address public addr = address(this);
  string public attributable;
  uint public totalPayees;


  constructor(
    string memory _attributable,
    address[] memory payees,
    uint256[] memory shares  
  ) PaymentSplitter( payees, shares ) public payable{
     attributable = _attributable;
     for (uint256 i = 0; i < payees.length; i++) {
         //_addPayee(payees[i], shares_[i]);
         newPayee(payees[i], shares[i]);
         totalPayees ++;
     }
  }

  function setAttributable(string memory a) public {
    attributable = a;
  }

  struct Payee {
      address tenedor;
      uint partes;
      //uint numFunders;
      //uint amount;
      //mapping (uint => Funder) funders;
  }
  
  uint numPayees;
  mapping (uint => Payee) public payees;
  
  function newPayee( address tenedor, uint partes ) public returns (uint payeeID) {
      payeeID = numPayees++; // payeeID is return variable
      // Creates new struct and saves in storage.
      //We leave out the mapping type.
      // payees[payeeID] = Payee(tenedor, partes, 0, 0);
      payees[payeeID] = Payee(tenedor, partes);
  }
  
  //function contribute(uint payeeID) public payable {
  //    Payee storage c = payees[payeeID];
  //    // Creates a new temporary memory struct, initialised with the given values
  //    // and copies it over to storage.
  //    // Note that you can also use Funder(msg.sender, msg.value) to initialise.
  //    //c.funders[c.numFunders++] = Funder({addr: msg.sender, amount: msg.value});
  //    //c.amount += msg.value;
  //}

}

