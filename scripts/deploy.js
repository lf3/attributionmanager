const hre = require("hardhat");
const fs = require('fs');

async function main() {

  const [
    owner,
    tenedor1,
    tenedor2,
    tenedor3
  ] = await hre.ethers.getSigners();

  const AttributionManager = await ethers.getContractFactory(
    "AttributionManager"
  );
  const attributionmanager = await AttributionManager.deploy(
    "Manager de Atribuciones",
  );
  await attributionmanager.deployed();
  console.log(
     ( await attributionmanager.name() ),
    "deployed to:",
     attributionmanager.address
  );
  fs.writeFile(
    'client/.env',
    'REACT_APP_AM_ADDRESS="' + attributionmanager.address + '"',
     function (err) {
      if (err) throw err;
       console.log('AM addres Saved!');
     }
   );

  // to test
  const tenedores = [
   tenedor1.address,
   tenedor2.address,
   tenedor3.address
  ];
  const partes =  [ 4, 3, 5 ];
  const cero = await attributionmanager.addSplitter(
    "https://soundcloud.com/lifo/en-la-dubida",
    tenedores,
    partes
  );

  const tenedores1 = [
   tenedor2.address,
   tenedor3.address
  ];
  const partes1 = [ 4, 5 ];
  const dos = await attributionmanager.addSplitter(
    "https://live.staticflickr.com/7721/16567171923_284e329476_b.jpg",
    tenedores1,
    partes1
  );

  const CompensationSplitter = await ethers.getContractFactory(
    "CompensationSplitter"
  );

  const csa = await attributionmanager.splittersAdrresses(0);
  const cs = await CompensationSplitter.attach( csa );
  const ts = await cs.totalShares();
  const tp = await cs.totalPayees();
  console.log(
    ( await cs.attributable() ),
    "deployed to:",
    csa,
    "\nTotal Shares:",
    ts.toString(),
    "\nTotal Payees:",
    tp.toString()
  );

  const csa1 = await attributionmanager.splittersAdrresses(1);
  const cs1 = await CompensationSplitter.attach( csa1 );
  const ts1 = await cs1.totalShares();
  const tp1 = await cs1.totalPayees();
  console.log(
    ( await cs1.attributable() ),
    "deployed to:",
    csa1,
    "\nTotal Shares:",
    ts1.toString(),
    "\nTotal Payees:",
    tp1.toString()
  );

}

// We recommend this pattern to be able to use async/await
// everywhere and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });


