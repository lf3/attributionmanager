# Deploy  
```
npx hardhat clean && npx hardhat compile && npx hardhat run scripts/deploy.js && rm client/src/contracts/* && ./copy-sc.sh
```

## Child Contract Address
```
$ npx hardhat console
> const M = await ethers.getContractFactory("AttributionManager")
> const m = await M.attach(CONTRACT_ADDRESS)

> const splitteraddress = await m.splitters(0)
> const C = await ethers.getContractFactory("CompensationSplitter")
> const c = await C.attach(splitteraddress)

(await m.splittersAdrresses(0)).toString()
```



## Cliente
Restartear el servidor web si cambia el addres del controact (.env)
