/* eslint-disable */
import React from 'react';

import { Provider } from 'react-redux';

import store from './store';
import Home from './Home';
//const AMA = process.env.REACT_APP_AM_ADDRESS
//console.log(AMA)

function App() {
  return (
    <Provider store={store}>
      <Home />
    </Provider>
  );
}

export default App;

