/* eslint-disable */
import React, {
  useState,
  useEffect,
} from 'react';

import {
  useDispatch,
} from 'react-redux';

import AttributionManager from './contracts/AttributionManager.json';
import CompensationSplitter from './contracts/CompensationSplitter.json';

const address = process.env.REACT_APP_AM_ADDRESS

import {
  setUserAddress,
  setIsAdmin,
  setMinterKey,
  setBurnerKey,
  setIsMinter,
  setIsBurner,
} from './actions/UserActions';

import NavBar from './components/NavBar';
// import AdminRole from './components/AdminRole';
import CompensationSplitters from './components/CompensationSplitters';

import Web3 from 'web3';

//const ethers = require('ethers');
//const Contract = require('web3-eth-contract');

// set provider for all later instances to use
// Contract.setProvider('ws://localhost:7545');
// Contract.setProvider( web3.currentProvider );

// The address from the above deployment example
// We connect to the Contract using a Provider, so we will only
// have read-only access to the Contract
//
//console.log(ethers);
//const contractFactory = new ethers.ContractFactory(
//  AttributionManager.abi,
//  AttributionManager.bytecode,
//);
//console.log(contractFactory.interface.functions);
//const a = contractFactory.interface.functions;
//const c = contractFactory.getDeployTransaction();
//console.log(c);

function Home() {

   const dispatch = useDispatch();
   const [ethBrowserError, setEthBrowserError] = useState(null);
   const [ethContractError, setEthContractError] = useState(null);
   const [account, setAccount] = useState(null);
   const [loading, setLoading] = useState(true);

   // Attribution Manager
   const [attributionmanager, setAttributionManager] = useState(null);
   const [AttrMngrAddress, setAttrMngrAddress] = useState(null);
   const [totalSplitters, setTotalSplitters] = useState(false);
   const [splitters, setSplitters] = useState(false);

   // Access 
   const [adminRole, setAdminRole] = useState(false);
   const [burnerRole, setBurnerRole] = useState(false);
   const [burnerRoleKey, setBurnerRoleKey] = useState('');
   const [minterRole, setMinterRole] = useState(false);
   const [minterRoleKey, setMinterRoleKey] = useState('');

   const [init, setInit] = useState(false);
 
   async function loadWeb3() {
     if (window.ethereum) {
       window.web3 = new Web3( window.ethereum );
       await window.ethereum.enable();
     } else if (window.web3) {
       window.web3 = new Web3(window.web3.currentProvider);
     } else {
       setEthBrowserError('Non-Ethereum browser detected');
     }
   }
 
   async function loadBlockChainData() {
     const { web3 } = window;
     const accounts = await web3.eth.getAccounts();
     setAccount(accounts[0]);
     dispatch(setUserAddress(accounts[0]));
     
     //const netWorkId = await web3.eth.net.getId();
     //const netWork = await web3.eth.net;
     //const netWorkData = AttributionManager.networks[netWorkId];

     //const network = ethers.providers.getNetwork('homestead');
     ///const network = ethers.providers.getNetwork('127.0.0.1:7545');
     //const provider = ethers.getDefaultProvider('homestead');

     // METaddressSK
     //const provider = new ethers.providers.Web3Provider(web3.currentProvider);
     
     // // e.g. HTTP provider
     // let currentProvider = new web3.providers.HttpProvider('http://localhost:8545');
     // let web3Provider = new ethers.providers.Web3Provider(currentProvider);
     //console.log(ethers);
     
     //const provider = ethers.providers.JsonRpcProvider('http://localhost:7545', ethers.networks.unspecified);
     //const provider = new ethers.providers.JsonRpcProvider('http://localhost:7545' );
     //const provider = new ethers.providers.JsonRpcProvider();
     //const netWorkId = network.chainId;
     //const networkAddress = network.ensAddress;

     if(address) {
       
       //console.log( networkAddress );
       //const AttrMngr = new ethers.Contract(
       //   address,
       //   AttributionManager.abi,
       //   provider
       //);
       
       console.log( "AttrMngr address:", address );
       const AttrMngr = new web3.eth.Contract(
         AttributionManager.abi,
         address
       );

       console.log( AttrMngr );
       //const am = await AttributionManager.attach( csa );

       setAttributionManager(AttrMngr);
       setAttrMngrAddress(address);
       //const { _address } = AttrMngr;
       const totalSplitters = await AttrMngr
         .methods.totalSplitters().call();
       setTotalSplitters(totalSplitters);

       var i;
       const splitters = []
       for (i = 0; i < totalSplitters ; i++) {
         const splitter = {}
         splitter.id = i;
         splitter.address = await AttrMngr.methods.splitters(i).call();
         const compensationSplitter = new web3.eth.Contract(
           CompensationSplitter.abi,
           splitter.address
         );
         splitter.instance = compensationSplitter;
         splitter.totalShares = await compensationSplitter
           .methods.totalShares().call();
         splitter.totalPayees = await compensationSplitter
           .methods.totalPayees().call();
         const totalReleased = await compensationSplitter
           .methods.totalReleased().call();
           splitter.totalReleased = web3.utils.fromWei(
            totalReleased
           );
         splitter.attributable = await compensationSplitter
           .methods.attributable().call();

         var e;
         splitter.payees = []
         for (e = 0; e < splitter.totalPayees; e++) {
           const payee = await compensationSplitter.methods.payees(
            e
           ).call();
           const soltado = await compensationSplitter.methods.released(
            payee.tenedor
           ).call();
           payee.soltado = web3.utils.fromWei(soltado);
	   console.log(payee.soltado);
           splitter.payees.push(payee);
         }
         splitters.push(splitter);
       }
       setSplitters(splitters);
       console.log(splitters);

       setLoading(false);
     } else {
       setEthContractError(
         'AttributionManager not deployed to detected network'
       );
     }
   }
   useEffect(() => {
     if (!init) {
       loadWeb3();
       loadBlockChainData();
       setInit(true);
     }
     return () => {
       console.log('Desmontando blockchain ...');
     };
   }, [init]);
 
  return (
   <div className="App">
     {ethBrowserError && (
       <>
         <p>{ ethBrowserError }</p>
       </>
     )}
     {ethContractError && (
       <>
         <p>{ ethContractError }</p>
       </>
     )}
     {loading && (
       <>
         <p>Loading ...</p>
       </>
     )}

     {!loading && (
       <>
         <NavBar
           AttrMngrAddress={AttrMngrAddress}
           splitters={splitters}
           totalSplitters={totalSplitters}
         />
       </>
     )}

     {!loading && (
       <>
         <CompensationSplitters
           splitters={splitters}
           totalSplitters={totalSplitters}
         />
       </>
     )}

     {adminRole && (
       <AdminRole
         attributionmanager={ attributionmanager }
         minterRoleKey={minterRoleKey}
         minterRole={minterRole}
         burnerRoleKey={burnerRoleKey}
         burnerRole={burnerRole}
         adminAddress={account}
       />
     )}
   </div>
   );

}
export default Home;




