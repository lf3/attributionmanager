/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';
//const AMA = process.env.REACT_APP_AM_ADDRESS
////const AMA = "0xB45B110641DeC072F1fECa36BBc4b02E1492e2bf"
//console.log(AMA)

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
