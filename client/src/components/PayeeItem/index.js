/* eslint-disable */
//import React from 'react';
import React, {
  useState,
  useEffect,
} from 'react';
import { useSelector } from 'react-redux';
  
function PayeeItem(props) {
  const [soltadoPayee, setSoltadoPayee] = useState();
  const payee = props.payee;

  useEffect(() => {
    setSoltadoPayee(payee.soltado);
    }, [payee.soltado]
  );

  async function relese(e) {
    e.preventDefault();
    const s = props.soltar( props.payee.tenedor );
    //setSoltadoPayee(payee.soltado);
  }
  return (
    <li>
      <b>Tenedor:</b>&nbsp;{ payee.tenedor }&nbsp;
      <b>Partes:</b>&nbsp;{ payee.partes }<br/>
      <button onClick={relese} type="submit">Soltar Pago</button>
      &nbsp; Soltado:&nbsp; { payee.soltado } 
    </li>
  );
}
export default PayeeItem;

