/* eslint-disable */
import React from 'react';
import { useSelector } from 'react-redux';

function NavBar({
  AttrMngrAddress,
}) {
  const {
    address, //user
    admin,
    minter,
    burner,
  } = useSelector((state) => state.UserReducer);
  return (
    <nav>
      <ul>
        <li>
          <b>Account address:</b>&nbsp;
          { address }
        </li>
        {admin && (
          <li> Sos admin!  </li>
        )}
        {minter && (
          <li> Sos minter!  </li>
        )}
        {burner && (
          <li> Sos burner!  </li>
        )}
        <li>
          <b>Attributions Manager address:</b>&nbsp;
          { AttrMngrAddress }
        </li>
      </ul>
    </nav>
  );
}
export default NavBar;

