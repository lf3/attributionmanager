/* eslint-disable */
//import React from 'react';

import React, {
  useState,
  useEffect,
} from 'react';

import {
  useSelector,
} from 'react-redux';

import Web3 from 'web3';

import PayeeItem from '../PayeeItem';

function SplitterItem( props ) {
  const splitter = props.value;
  const [soltado, setSoltado] = useState('');
  const [pago, setPago] = useState('');
  const [pagador, setPagador] = useState('');

  useEffect(() => {
    setSoltado(splitter.totalReleased);
    }, [splitter.totalReleased]
  );

  const {
    address, //user
    admin,
    minter,
    burner,
  } = useSelector( ( state ) => state.UserReducer );

  async function soltar( tenedor ){
    const soltando = await splitter.instance.methods.release(
     tenedor
    ).send({from: address });
  }
  splitter.instance.events.PaymentReceived(
    //{
    //  filter: {
    //    myIndexedParam: [20,23],
    //    myOtherIndexedParam: '0x123456789...'
    //  },
    //  // Using an array means OR: e.g. 20 or 23
    //  fromBlock: 0
    //},
    //function(error, event){
    //  console.log(event);
    //}
  )
  .on('data', ( r ) => {
    setPago( Web3.utils.fromWei( r.returnValues.amount) );
    setPagador( r.returnValues.from);
    //const { blockHash } = rec;
  })
  .on('changed', function(event){
    // remove event from local database
  })
  .on('error', console.error);

  splitter.instance.events.PaymentReleased()
  .on('data', ( r ) => {
    setSoltado(splitter.totalReleased);
  })
  .on('changed', function(event){
    // remove event from local database
    setSoltado(splitter.totalReleased);
  })
  .on('error', console.error);

  const payees = splitter.payees;
  const payeeItems = payees.map( ( payee, index ) =>
    <PayeeItem className="payee" key={index} payee={payee} soltar={soltar} />
  );
  return (
    <li>
      <b>Splitter { splitter.id }:</b>&nbsp;
         { splitter.address }<br/>
      <b>Atribuible:</b>&nbsp;{ splitter.attributable }<br/>
      <b>Tenedores:</b>&nbsp;{ splitter.totalPayees }<br/>
      <b>Partes:</b>&nbsp;{ splitter.totalShares }<br/>
      <b>Soltado:</b>&nbsp;{ soltado }<br/>
      <ul> {payeeItems} </ul>
      <b>Recibido :</b>&nbsp;{ pago }&nbsp;
      <b>Pagador:</b>&nbsp;{ pagador }<br/>
      <hr />
    </li>
  );
}
export default SplitterItem;


