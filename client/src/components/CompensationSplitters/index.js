/* eslint-disable */
import React from 'react';
import { useSelector } from 'react-redux';

import SplitterItem from '../SplitterItem';

function CompensationSplitters({
  splitters,
  totalSplitters,
}){
 
  return (
    <div>
      Cantidad de CompensationSplitters:&nbsp;
      { totalSplitters }
      <Splitters splitters={splitters} />
    </div>
  );
}
export default CompensationSplitters;

function Splitters( props ) {
  const splitters = props.splitters;
  const splittersItems = splitters.map( (splitter, index) =>
    <SplitterItem className="splitter" key={index} value={splitter} />
  );
  return (
    <ul>
      {splittersItems}
    </ul>
  );
}

// function ListItem(props) {
//   const value = props.value;
//   return (
//     <li>
//       {value} 
//     </li>
//   );
// }
// 
// function NumberList(props) {
//   const numbers = props.numbers;
//   const listItems = numbers.map( (number, index) =>
//     <ListItem key={index} value={number} />
//   );
//   return (
//     <ul>
//       { listItems }
//     </ul>
//   );
// }
